<?php
namespace Etsoftware\Qcode;
use Etsoftware\Lib\Image;

abstract class Base
{
	protected $img = null;
	protected $width = null;
	protected $height = null;
	public function __construct($width=100, $height=60){
		$this->img = new Image($width, $height);
		$this->width = $width;
		$this->height = $height;
	}
    /**
     * create image by string
     * @param string $method
     * @param array $attributes 
     * @return mixed
     */	
	abstract public function create($str, $fileName=null);
	protected function drawLine($x, $y){
		$y2 = $this->img->height() - $y*2-12;
		$this->img->drawLine($x, $y, $x, $y2);
	}
	protected function zoom($data, $zoom=1){
		if($zoom<2)return $data;
		$reVal = [];
		foreach ($data as $k => $v) {
			array_push($reVal, $v);
			for($i=1; $i<$zoom; $i++){
				array_push($reVal, $v);
			}
		}
		return $reVal;
	}
	public function getImage(){
		return $this->img->getImage();
	}
	protected function drawData($data, $str=null){
		if(!$data){ return $this; }
		$x = 10; $y = 5;
		$bw = intval(($this->width-$x*2) / count($data));
		$data = $this->zoom($data, $bw);
		foreach ($data as $k => $v) {
			if($v){ $this->drawLine($x, $y); }
			$x++;
		}
		if($str){
			$fontsize = 1;
			$ifw = imagefontwidth($fontsize);
			$x = $this->img->width()/2-$ifw*strlen($str)/2;
			$y = $this->img->height() - $y-12;
			// $this->img->drawText($str, 5, 1, $x, $y);
			$this->img->drawString($str, $fontsize, $x, $y);
		}
	}

}