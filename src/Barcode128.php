<?php
namespace Etsoftware\Qcode;

class Barcode128 extends Base
{
  private $type = null;
  private $code = null;
  private $text = null;
  private $tag = array(
    'start'=>['A'=>'211412', 'B'=>'211214', 'C'=>'211232'],
    'end'=>['A'=>'2331112', 'B'=>'2331112', 'C'=>'2331112']
  );
  private $codes = array(
      ["A"=>"SP", "B"=>"SP", "C"=>"00", "Code"=>"212222", "string"=>"SP"],
      ["A"=>"!", "B"=>"!", "C"=>"01", "Code"=>"222122", "string"=>"!"],
      ["A"=>"\"", "B"=>"\"", "C"=>"02", "Code"=>"222221", "string"=>"\""],
      ["A"=>"#", "B"=>"#", "C"=>"03", "Code"=>"121223", "string"=>"#"],
      ["A"=>"$", "B"=>"$", "C"=>"04", "Code"=>"121322", "string"=>"$"],
      ["A"=>"%", "B"=>"%", "C"=>"05", "Code"=>"131222", "string"=>"%"],
      ["A"=>"&", "B"=>"&", "C"=>"06", "Code"=>"122213", "string"=>"&"],
      ["A"=>"'", "B"=>"'", "C"=>"07", "Code"=>"122312", "string"=>"'"],
      ["A"=>"(", "B"=>"(", "C"=>"08", "Code"=>"132212", "string"=>"("],
      ["A"=>")", "B"=>")", "C"=>"09", "Code"=>"221213", "string"=>")"],
      ["A"=>"*", "B"=>"*", "C"=>"10", "Code"=>"221312", "string"=>"*"],
      ["A"=>"+", "B"=>"+", "C"=>"11", "Code"=>"231212", "string"=>"+"],
      ["A"=>",", "B"=>",", "C"=>"12", "Code"=>"112232", "string"=>","],
      ["A"=>"-", "B"=>"-", "C"=>"13", "Code"=>"122132", "string"=>"-"],
      ["A"=>".", "B"=>".", "C"=>"14", "Code"=>"122231", "string"=>"."],
      ["A"=>"/", "B"=>"/", "C"=>"15", "Code"=>"113222", "string"=>"/"],
      ["A"=>"0", "B"=>"0", "C"=>"16", "Code"=>"123122", "string"=>"0"],
      ["A"=>"1", "B"=>"1", "C"=>"17", "Code"=>"123221", "string"=>"1"],
      ["A"=>"2", "B"=>"2", "C"=>"18", "Code"=>"223211", "string"=>"2"],
      ["A"=>"3", "B"=>"3", "C"=>"19", "Code"=>"221132", "string"=>"3"],
      ["A"=>"4", "B"=>"4", "C"=>"20", "Code"=>"221231", "string"=>"4"],
      ["A"=>"5", "B"=>"5", "C"=>"21", "Code"=>"213212", "string"=>"5"],
      ["A"=>"6", "B"=>"6", "C"=>"22", "Code"=>"223112", "string"=>"6"],
      ["A"=>"7", "B"=>"7", "C"=>"23", "Code"=>"312131", "string"=>"7"],
      ["A"=>"8", "B"=>"8", "C"=>"24", "Code"=>"311222", "string"=>"8"],
      ["A"=>"9", "B"=>"9", "C"=>"25", "Code"=>"321122", "string"=>"9"],
      ["A"=>":", "B"=>":", "C"=>"26", "Code"=>"321221", "string"=>":"],
      ["A"=>";", "B"=>";", "C"=>"27", "Code"=>"312212", "string"=>";"],
      ["A"=>"<", "B"=>"<", "C"=>"28", "Code"=>"322112", "string"=>"<"],
      ["A"=>"=", "B"=>"=", "C"=>"29", "Code"=>"322211", "string"=>"="],
      ["A"=>">", "B"=>">", "C"=>"30", "Code"=>"212123", "string"=>">"],
      ["A"=>"?", "B"=>"?", "C"=>"31", "Code"=>"212321", "string"=>"?"],
      ["A"=>"@", "B"=>"@", "C"=>"32", "Code"=>"232121", "string"=>"@"],
      ["A"=>"A", "B"=>"A", "C"=>"33", "Code"=>"111323", "string"=>"A"],
      ["A"=>"B", "B"=>"B", "C"=>"34", "Code"=>"131123", "string"=>"B"],
      ["A"=>"C", "B"=>"C", "C"=>"35", "Code"=>"131321", "string"=>"C"],
      ["A"=>"D", "B"=>"D", "C"=>"36", "Code"=>"112313", "string"=>"D"],
      ["A"=>"E", "B"=>"E", "C"=>"37", "Code"=>"132113", "string"=>"E"],
      ["A"=>"F", "B"=>"F", "C"=>"38", "Code"=>"132311", "string"=>"F"],
      ["A"=>"G", "B"=>"G", "C"=>"39", "Code"=>"211313", "string"=>"G"],
      ["A"=>"H", "B"=>"H", "C"=>"40", "Code"=>"231113", "string"=>"H"],
      ["A"=>"I", "B"=>"I", "C"=>"41", "Code"=>"231311", "string"=>"I"],
      ["A"=>"J", "B"=>"J", "C"=>"42", "Code"=>"112133", "string"=>"J"],
      ["A"=>"K", "B"=>"K", "C"=>"43", "Code"=>"112331", "string"=>"K"],
      ["A"=>"L", "B"=>"L", "C"=>"44", "Code"=>"132131", "string"=>"L"],
      ["A"=>"M", "B"=>"M", "C"=>"45", "Code"=>"113123", "string"=>"M"],
      ["A"=>"N", "B"=>"N", "C"=>"46", "Code"=>"113321", "string"=>"N"],
      ["A"=>"O", "B"=>"O", "C"=>"47", "Code"=>"133121", "string"=>"O"],
      ["A"=>"P", "B"=>"P", "C"=>"48", "Code"=>"313121", "string"=>"P"],
      ["A"=>"Q", "B"=>"Q", "C"=>"49", "Code"=>"211331", "string"=>"Q"],
      ["A"=>"R", "B"=>"R", "C"=>"50", "Code"=>"231131", "string"=>"R"],
      ["A"=>"S", "B"=>"S", "C"=>"51", "Code"=>"213113", "string"=>"S"],
      ["A"=>"T", "B"=>"T", "C"=>"52", "Code"=>"213311", "string"=>"T"],
      ["A"=>"U", "B"=>"U", "C"=>"53", "Code"=>"213131", "string"=>"U"],
      ["A"=>"V", "B"=>"V", "C"=>"54", "Code"=>"311123", "string"=>"V"],
      ["A"=>"W", "B"=>"W", "C"=>"55", "Code"=>"311321", "string"=>"W"],
      ["A"=>"X", "B"=>"X", "C"=>"56", "Code"=>"331121", "string"=>"X"],
      ["A"=>"Y", "B"=>"Y", "C"=>"57", "Code"=>"312113", "string"=>"Y"],
      ["A"=>"Z", "B"=>"Z", "C"=>"58", "Code"=>"312311", "string"=>"Z"],
      ["A"=>"[", "B"=>"[", "C"=>"59", "Code"=>"332111", "string"=>"["],
      ["A"=>"\\", "B"=>"\\", "C"=>"60", "Code"=>"314111", "string"=>"\\"],
      ["A"=>"]", "B"=>"]", "C"=>"61", "Code"=>"221411", "string"=>"]"],
      ["A"=>"^", "B"=>"^", "C"=>"62", "Code"=>"431111", "string"=>"^"],
      ["A"=>"_", "B"=>"_", "C"=>"63", "Code"=>"111224", "string"=>"_"],
      ["A"=>"NUL", "B"=>"`", "C"=>"64", "Code"=>"111422", "string"=>"`"],
      ["A"=>"SOH", "B"=>"a", "C"=>"65", "Code"=>"121124", "string"=>"a"],
      ["A"=>"STX", "B"=>"b", "C"=>"66", "Code"=>"121421", "string"=>"b"],
      ["A"=>"ETX", "B"=>"c", "C"=>"67", "Code"=>"141122", "string"=>"c"],
      ["A"=>"EOT", "B"=>"d", "C"=>"68", "Code"=>"141221", "string"=>"d"],
      ["A"=>"ENQ", "B"=>"e", "C"=>"69", "Code"=>"112214", "string"=>"e"],
      ["A"=>"ACK", "B"=>"f", "C"=>"70", "Code"=>"112412", "string"=>"f"],
      ["A"=>"BEL", "B"=>"g", "C"=>"71", "Code"=>"122114", "string"=>"g"],
      ["A"=>"BS", "B"=>"h", "C"=>"72", "Code"=>"122411", "string"=>"h"],
      ["A"=>"HT", "B"=>"i", "C"=>"73", "Code"=>"142112", "string"=>"i"],
      ["A"=>"LF", "B"=>"j", "C"=>"74", "Code"=>"142211", "string"=>"j"],
      ["A"=>"VT", "B"=>"k", "C"=>"75", "Code"=>"241211", "string"=>"k"],
      ["A"=>"FF", "B"=>"l", "C"=>"76", "Code"=>"221114", "string"=>"l"],
      ["A"=>"CR", "B"=>"m", "C"=>"77", "Code"=>"413111", "string"=>"m"],
      ["A"=>"SO", "B"=>"n", "C"=>"78", "Code"=>"241112", "string"=>"n"],
      ["A"=>"SI", "B"=>"o", "C"=>"79", "Code"=>"134111", "string"=>"o"],
      ["A"=>"DLE", "B"=>"p", "C"=>"80", "Code"=>"111242", "string"=>"p"],
      ["A"=>"DC1", "B"=>"q", "C"=>"81", "Code"=>"121142", "string"=>"q"],
      ["A"=>"DC2", "B"=>"r", "C"=>"82", "Code"=>"121241", "string"=>"r"],
      ["A"=>"DC3", "B"=>"s", "C"=>"83", "Code"=>"114212", "string"=>"s"],
      ["A"=>"DC4", "B"=>"t", "C"=>"84", "Code"=>"124112", "string"=>"t"],
      ["A"=>"NAK", "B"=>"u", "C"=>"85", "Code"=>"124211", "string"=>"u"],
      ["A"=>"SYN", "B"=>"v", "C"=>"86", "Code"=>"411212", "string"=>"v"],
      ["A"=>"ETB", "B"=>"w", "C"=>"87", "Code"=>"421112", "string"=>"w"],
      ["A"=>"CAN", "B"=>"x", "C"=>"88", "Code"=>"421211", "string"=>"x"],
      ["A"=>"EM", "B"=>"y", "C"=>"89", "Code"=>"212141", "string"=>"y"],
      ["A"=>"SUB", "B"=>"z", "C"=>"90", "Code"=>"214121", "string"=>"z"],
      ["A"=>"ESC", "B"=>"{", "C"=>"91", "Code"=>"412121", "string"=>"{"],
      ["A"=>"FS", "B"=>"|", "C"=>"92", "Code"=>"111143", "string"=>"|"],
      ["A"=>"GS", "B"=>"}", "C"=>"93", "Code"=>"111341", "string"=>"}"],
      ["A"=>"RS", "B"=>"~", "C"=>"94", "Code"=>"131141", "string"=>"~"],
      ["A"=>"US", "B"=>"DEL", "C"=>"95", "Code"=>"114113", "string"=>"DEL"],
      ["A"=>"FNC 3", "B"=>"FNC 3", "C"=>"96", "Code"=>"114311", "string"=>"?"],
      ["A"=>"FNC 2", "B"=>"FNC 2", "C"=>"97", "Code"=>"411113", "string"=>"ü"],
      ["A"=>"SHIFT", "B"=>"SHIFT", "C"=>"98", "Code"=>"411311", "string"=>"é"],
      ["A"=>"CODE C", "B"=>"CODE C", "C"=>"99", "Code"=>"113141", "string"=>"a"],
      ["A"=>"CODE B", "B"=>"FNC 4", "C"=>"CODE B", "Code"=>"114131", "string"=>"?"],
      ["A"=>"FNC 4", "B"=>"CODE A", "C"=>"CODE A", "Code"=>"311141", "string"=>"à"],
      ["A"=>"FNC 1", "B"=>"FNC 1", "C"=>"FNC 1", "Code"=>"411131", "string"=>"?"],
  );
  public function __construct($type='', $width=null, $height=null){
    $type = strtoupper($type);
    if (in_array($type, array('A', 'B', 'C'))){ 
      $this->type = $type; 
    }else{
      // $this->type = $this->getType();
    }
    parent::__construct($width??300, $height??100);
  }
  private function getType($str){
    $type="B";
    if(preg_match("/^(\d\d){1,}$/m", $str)){
      $type="C";
    }else if(preg_match("/^[A-Z\d]+$/m", $str)){
      $type="A";
    }else if(preg_match("/^[A-Za-z\d]+$/m", $str)){
      $type="B";
    }else {
      $type="B";
    }
    return $type;
  }
  /**
   * create image by string
   * @param string $method
   * @param array $attributes 
   * @return mixed
        以95270078为例： CODE128A，开始位对应的ID为103，第1位数据9对应的ID为25, 
        第2位数据5对应的ID为21，依此类推，可以计算校验位为： 
        （103＋1×25＋2×21＋3×18＋4×23＋5×16＋6×16＋7×23＋8×24）% 103 = 21。
        即校验位的ID为21。
   */   
  private function checkCode($str, $type="B"){
    $startIdx = ['A'=>103, 'B'=>104, 'C'=>105][$type]??0;
    if(!$startIdx) return $startIdx;
    $idx = $startIdx;
    $stp = $this->type=='C'?2:1; $n=1;
    for($i=0, $l = strlen($str); $i<$l; $i+=$stp) {
      $c = substr($str, $i, $stp);
      foreach ($this->codes as $k => $v) {
        if($v[$type] != $c)continue;
        $idx += $k* $n;
        $n++;
        break;
      }
    }
    $idx = $idx % 103;
    $code = $this->codes[$idx];
    return $code['Code'];
  }
  private function createImage()
  {
    $data = [];
    if($this->type == null){
      $this->type = $this->getType($this->code);
    }
    //Start
    array_push($data, $this->tag['start'][$this->type]);
    //Data
    $stp = $this->type=='C'?2:1;
    for($i=0, $l = strlen($this->code); $i<$l; $i+=$stp) {
        $c = substr($this->code, $i, $stp);
        foreach ($this->codes as $k => $v) {
          if($v[$this->type] != $c)continue;
          array_push($data, $v['Code']);
          break;
        }
    }
    //Check Code
    array_push($data, $this->checkCode($this->code, $this->type));
    //End
    array_push($data, $this->tag['end'][$this->type]);
    $ldata = [];
    foreach ($data as $k => $v) {
      for($i=0, $l = strlen($v); $i<$l; $i++) {
        $count = substr($v, $i, 1)*1;
        $c = ($i%2==0)?1:0;
        for($j=0; $j<$count; $j++) {
          array_push($ldata, $c);
        }
      }
    }
    $this->drawData($ldata, $this->text);// 文字
  }
  public function setContent($code, $text=null)
  {
    $this->code = $code;
    $this->text = $text??$code;
  }
  /**
   * create image by string
   * @param string $method
   * @param array $attributes 
   * @return mixed
   */ 
  public function create($str, $fileName=null)
  {
    if($str) $this->setContent($str, $str);
    $this->createImage();
    $this->img->toJpeg($fileName);// 输出jpg
    die;
  }
}

