<?php
namespace Etsoftware\Qcode;
use Etsoftware\Lib\Image;

class Vcode 
{
  private $width = 100;
  private $height = 25;
  private $key = '';
  public function __construct($width=0, $height=0, $key=""){
    if($width)$this->width = $width;
    if($height)$this->height = $height;
    $this->key = "VCODE_".hash('sha1', __CLASS__).$key;
    session_start();
  }

  /**
   * verification 
   * @param string $method
   * @param array $attributes 
   * @return mixed
   */	
	public function verification($str){
    if(!isset($_SESSION[$this->key]))return false;
    $ret = (strtolower($_SESSION[$this->key]) == strtolower($str));
    unset($_SESSION[$this->key]);
    return $ret;
  }
  /**
   * create image by string
   * @param string $method
   * @param array $attributes 
   * @return mixed
   */ 
  public function create($str=null)
	{
    $str = $str?$str:$this->getRandString(6);
    $_SESSION[$this->key] = $str;
		$img = new Image($this->width, $this->height);
    $img->drawFill(0, 0, rand(0xa50000, 0xee0000)|rand(0xa5, 0xee)<<8|rand(0xa5, 0xee));// 
    $this->getRand(1, ($this->width*$this->height)/10, function($p)use($img){
      $img->drawPixel($p[0]['x'], $p[0]['y'], $p[0]['color']); // 像素
    });
    $this->getRand(1, 3, function($p)use($img){
      $img->drawRectangle($p[0]['x'], $p[0]['y'], null, null, $p[0]['color']); // 距形
    });

    $this->getRand(1, 3, function($p)use($img){
      $img->drawEllipse($p[0]['x'], $p[0]['y'], null, null, $p[0]['color']); // 椭圆
    });

    $this->getRand(0, 3, function($p)use($img){
      $img->drawCircle($p[0]['x'], $p[0]['y'], 10, $p[0]['color']); // 圆
    });

    $this->getRand(0, 3, function($p)use($img){
      $img->drawSquare($p[0]['x'], $p[0]['y'], 10, $p[0]['color']); // 
    });
    $this->getRand(0, 3, function($p)use($img){
      $img->drawPoint($p[0]['x'], $p[0]['y'], rand(0, 9), $p[0]['color']); // 点
    });
    $this->getRand(1, 3, function($p)use($img){
      $img->drawFilledarc($p[0]['x'], $p[0]['y'], 10, 10, rand(0, 360), rand(0, 180), $p[0]['color']); // 扇型
    });
    $fs = 16;
    $color = rand(0xa50000, 0xee0000)|rand(0xa5, 0xee)<<8|rand(0xa5, 0xee);
		$img->drawFill($this->width/3, 10, $color);// 
    $len = strlen($str);
    $r = rand(0x00, 0x44); $g = rand($r, 0x44); $b = rand($r, $g);
    $color = $r<<16|$g<<8|$b;
    $img->drawText(substr($str, 0, $len/2), $fs, 350, 5, 18, $color, null, 5);// 文字
    $r = rand(0x00, 0x44); $g = rand($r, 0x44); $b = rand($r, $g);
    $color = $r<<16|$g<<8|$b;
    $img->drawText(substr($str, $len/2), $fs, 5, $this->width/2, 22, $color, null, 5);// 文字
    
    $this->getRand(2, 5, function($p)use($img){
      $img->drawLine($p[0]['x'], $p[0]['y'], $p[1]['x'], $p[1]['y'], $p[0]['color']); // 线 
    });
    $this->getRand(1, 5, function($p)use($img){
      $color = rand(0xa50000, 0xee0000)|rand(0xa5, 0xee)<<8|rand(0xa5, 0xee);
      $img->drawFill($p[0]['x'], 0, $color);//
    });

		$img->toJpeg();// 输出jpg
		die;
	}
  private function getRandString($len){
      $asc =[[48, 57], [65, 90], [97, 122] ];
      $reVal = '';
      $len = $len?$len:6;
      for($i=0; $i<$len; $i++){
         $a = $asc[rand(0, count($asc)-1)];
         $c = rand($a[0], $a[1]);
         $reVal .= chr($c);
      }
      return $reVal;
  }
  private function getRand($min=0, $max=2, $callback){
    $len = rand($min, $max);
    for($j=0; $j<$len; $j++){
      $p = [];
      for($i=0; $i<3; $i++){
        array_push($p, $this->getPoint());
      }
      $callback->call($this, $p);
    }
    return $this;
  }
  private function getPoint(){
    return [
      'x'=> rand(0, $this->width)
      , 'y'=> rand(0, $this->height)
      , 'color'=> rand(0x000000, 0xffffff)
    ];
  }
}