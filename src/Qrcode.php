<?php
namespace Etsoftware\Qcode;
// https://blog.csdn.net/a22222259/article/details/98173091
// https://www.cnblogs.com/sddai/p/5675041.html
class Qrcode extends Base
{
	private $version;
	public function __construct($version=3, $width=270, $height=270){
		$this->version = intval($version);
		parent::__construct($width, $height);
	}

	/**
	* create image by string
	* @param string $method
	* @param array $attributes 
	* @return mixed
	*/
	public function create($str, $fileName=null){
		$this->drawGrid();
		$this->drawFunctionPattern();
		$this->drawEncodingRegion();


		return $this->img->toJpeg($fileName);// 输出jpg
	}
	/**
	* Funcation Pattenrn
	* @return mixed
	*/
	private function drawFunctionPattern(){
		$this->drawFinderPattern();
		$this->drawTimingPatterns();
		$this->drawDrakModule();
		$this->drawAlignmentPattern();
	}
	/**
	* Encoding Region
	* @return mixed
	*/
	private function drawEncodingRegion(){
		// $this->drawFormatInformation();
		// $this->drawVersionInformation();
		// $this->drawDataAndErrorCorrectionCodewords();
		
	}
	/**
	* Alignment Pattern
	* @return mixed
	*/
	private function drawAlignmentPattern(){
		if($this->version<2)return ;

	}
	/**
	* draw Drak Module
	* @return mixed
	*/
	private function drawDrakModule(){
		$vd = $this->getVersionDivisions($this->version);
		$p = $this->getVersionPoint(8, $vd-8);
		$this->drawPixel($p['x'], $p['y']);
	}
	/**
	* draw timing patterns
	* @return mixed
	*/
	private function drawTimingPatterns(){
		$vd = $this->getVersionDivisions($this->version);
		$s = 7+1;
		$count = $vd-$s*2;
		$color = 0x000000;
		while($count--){
			$p = $this->getVersionPoint($s+$count, $s-2);
			$this->drawPixel($p['x'], $p['y'], $color);

			$p = $this->getVersionPoint($s-2, $s+$count);
			$this->drawPixel($p['x'], $p['y'], $color);

			$color ^= 0xffffff;
		}
	}
	/**
	* draw Position detection map
	* @return mixed
	*/
	private function drawFinderPattern(){
		$vd = $this->getVersionDivisions($this->version);
		$v = $this->drawRect(0, 0);
		$v = $this->drawRect($vd-7, 0);
		$v = $this->drawRect(0, $vd-7);

		$v = $this->drawRect($vd-9, $vd-9, 5);
	}
	/**
	* get version divisions
	* @param string $method
	* @param array $attributes 
	* @return mixed
	*/
	private function getVersionDivisions($version=1){
		return ($version-1)*4 + 21;
	}
	private function getVersionPoint($x=0, $y=0){
		$d = $this->getVersionDivisions($this->version);
		$w = $this->getPixelWidth();
		$padding = ($this->width - $w*$d)/2;
		return ['x'=>$padding+$x*$w, 'y'=>$padding+$y*$w];
	}
	private function getPixelWidth(){
		$d = $this->getVersionDivisions($this->version);
		return intval($this->width/$d);
	}
	/**
	* draw net divisions
	* @return mixed
	*/	
	private function drawGrid(){
		$d = $this->getVersionDivisions($this->version);
		$w = $this->getPixelWidth();
		$padding = ($this->width - $w*$d)/2;
		$x = $y = $padding;
		$color = 0xe3e3e3;
		$fc = 0x333333;
		$fs = 5;
		for($i=0; $i<$d+10; $i++){
			$this->img->drawLine(0, $y+$i*$w, $this->width, $y+$i*$w, $color);
			$this->img->drawLine($x+$i*$w, 0, $x+$i*$w, $this->height, $color);

			// $this->img->drawText($i, $fs, 0, $x+$i*$w, $y+$d*$w, $fc);
			// $this->img->drawText($i, $fs, 0, 0, $y+$i*$w, $fc);
		}
	}
	private function drawPixel($x=0, $y=0, $color=null){
		$w = $this->getPixelWidth();
		$this->img->drawRectangle($x, $y, $x+$w, $y+$w, $color);
		$this->img->drawFill($x+1, $y+1, $color);
	}
	private function drawRect($x, $y, $count=7){
		$n = 0;
		$rw = $count;
		while ( $rw > 1) {
			$len = $rw;
			// dump($len);
			$nx = $x+$n*2;
			$ny = $y+$n*2;
			$n++;
			while ($len--) {
				$p = $this->getVersionPoint($nx+$len, $ny);
				$this->drawPixel($p['x'], $p['y']);

				$p = $this->getVersionPoint($nx+$rw-1, $ny+$len);
				$this->drawPixel($p['x'], $p['y']);


				$p = $this->getVersionPoint($nx+$len, $ny+$rw-1);
				$this->drawPixel($p['x'], $p['y']);
				
				$p = $this->getVersionPoint($nx, $ny+$len);
				$this->drawPixel($p['x'], $p['y']);
			}
			$rw -= 4;
		}
		$p = $this->getVersionPoint($x+intval($count/2), $y+intval($count/2));
		$this->drawPixel($p['x'], $p['y']);		

	}

}