<?php
require __DIR__.'/../vendor/autoload.php';

$code = "1234567890";
$Barcode128 = new Barcode128('a', 300, 30); // 采用barcode128a  生成300x30的图形条形码
// $Barcode128 = new Barcode128('b'); // 采用barcode128b 
// $Barcode128 = new Barcode128('c'); // 采用barcode128c 
$Barcode128->create($code); // 生成并输出
// $Barcode128->create($code, "/var/temp/vcode.png"); // 生成并保存到文件

$Barcode128->setContent($code, "BARCODE128"); // 设置内容
