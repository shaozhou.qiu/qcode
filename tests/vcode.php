<?php
require __DIR__.'/../vendor/autoload.php';

$vcode = new Vcode();
$vcode->create(); // 生成验证码图片并缓存到session

// 不区分大小写
// $code = $_GET['code']??'';
// $ret = $vcode->verification($code); // 验证
// if($ret){
// 	echo "success";
// }else{
// 	echo "failure";
// }
